# Open Index Chain

Here we propose to rebrand FLO as the Open Index Chain to better convey the vision, missions, and values of the development team and community. 

In particular, this is a sign of our commitment to solidify the FLO's position as the public space in the crypto community and the Internet.

>This is a proposal. There is no need to be alarmed for good or bad. At the time of writing and publishing, the pools, projects, exchanges, devs, and community have yet to discuss the topics at large.

>This proposal is also independent of the FLO foundation. The ongoing efforts to build a long waited Trezor support for FLO won't be affected by these plans.

## Rebrand

FLO's primary use nowadays is to store information on a blockchain. Users and developers can use the `flodata` field in the transaction to send a message or register information. In particular, by using the Open Index Project. By following the Open Index Protocol to build Open Records, users can insert data indexed by distributed instances of the Open Index Database (see the Open Index Project proposal). 

With so many concepts of *openness*, we think it is time to have our name reflecting our goals.

For this reason, as the first step in that direction, we suggest rebranding FLO as:

**Open Index Chain**

A few important considerations before we move on:

1) The Open Index Chain project remains distributed as FLO has always been and it is independent, in governance, from the Open Index Project. It is, however, our intention to signal our alignment with the ideals of the Open Index Project.

1.1) Alexandria is a strong supporter of these two distributed project, but it does not have more authority in its governance than other invested contributors.

2) The name reflects its openness. It is an encouragement to developers to use the Open Index Protocol to store and access data in the chain, albeit not enforced by any protocol rules.

3) This is a simple rebrand. It will be a hard fork only to change the name of the coin. During the rebrand, the only planned change besides in the core code is to reflect the Bitcoin's original development.

## Vision
To create, support and promote a digital public space.

## Mission
We seek to solidify as the top solution for public storage of metadata in the crypto community by increasing the security of the chain, keeping the code safe and promoting its usage.

## Values
We value, above all, freedom of expression, while providing tools for accountability to the best of our abilities.

## New logo

We will bring a few possibilities to a referendum by the community. The primary branding target is the theme "sun" or "light" to convey the possible enlightenment that can be achieved by uncensored knowledge.

## Dateless roadmap

* Rebrand of FLO to Open Index Chain - OIC.
    * Search for a new logo.
    * Discussion about vision, mission, and values.
* Inclusion of DDX to OIC official wallet.

## FAQ

### Why dateless?
One of the significant aspects of the last-gen crypto was roadmaps. Those made sense when initial coin offers (ICO's) were the norm and earlier supporters were now somewhat a stakeholder of the project. The investors needed to know what they were investing to and the developers received adopted currency to be able to fuel the development of that roadmap. Roadmaps served as a contract.

FLO had a different path. Skyangel, an anonymous developer, launched Florincoin, FLO's original name, without ICO, pre-mining of any of the shenanigans to raise money for development. The Alexandria team, a private initiative, then developed the Open Index Protocol to fit the specifications of FLO. Alexandria saw itself invested in funding a lot of the core development of FLO and gathered funds with the community to do so together with the FLO's lead developer Joseph Fiscella.

Despite the community's demand for deadlines, there was never a contract between the developing team and the coin holders. People buying FLO on exchanges never really directly funded any of the core development. And that is a good thing.

Moving forward, we are not offering tokens to fund the development described. For that reason, we are seeking to accomplish the roadmap as fast as we can deliver with the funds we can raise. Anyone who agrees can contribute to speeding the development, with funds or work.

## What is DDX?

DDX stands for Distributed Database template, and the title is exactly what it does.

DDX is a software package built from combining FLO, OIP, and IPFS to create an ElasticSearch instance of a database based on the Open Records published in the FLO blockchain.

DDX offers the ability to publish, browse or for Open Records, build new tables in the Open Index Database and more.

DDX can also work as a boilerplate for custom platforms/gateways to display and/or commercialize one or more types of Open Records.
